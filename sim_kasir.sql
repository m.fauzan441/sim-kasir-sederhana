-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Jul 2019 pada 18.55
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sim_kasir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_menu`
--

CREATE TABLE `kategori_menu` (
  `id` int(1) NOT NULL,
  `kategori` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_menu`
--

INSERT INTO `kategori_menu` (`id`, `kategori`) VALUES
(1, 'Drinks'),
(3, 'Dessert'),
(8, 'Coffe'),
(9, 'Pasta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `kategori_menu_id` int(2) DEFAULT NULL,
  `menu` varchar(50) DEFAULT NULL,
  `harga` int(6) DEFAULT NULL,
  `picture` varchar(53) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `kategori_menu_id`, `menu`, `harga`, `picture`, `status`) VALUES
(5, 2, 'Beverage 1', 45000, 'ScanKTP.jpg', 1),
(6, 3, 'DESSERT 1', 25000, '1.jpg', 1),
(7, 3, 'Desert 2', 26500, '3.jpg', 0),
(8, 3, 'Desert 3', 30000, '2.jpg', 1),
(9, 9, 'Pasta 1', 32500, '12.jpg', 1),
(10, 9, 'Pasta 2', 35000, '22.jpg', 1),
(11, 9, 'Pasta 3', 45000, '32.jpg', 1),
(12, 1, 'Drink 1', 38000, 'milkshake_3.jpg', 1),
(13, 1, 'Drink 2', 21000, 'milkshake.jpg', 1),
(14, 1, 'Drinks 3', 22000, 'milkshake2.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `payment`
--

CREATE TABLE `payment` (
  `no_pemesanan` varchar(20) NOT NULL,
  `total_harga` varchar(8) DEFAULT NULL,
  `total_kembalian` varchar(6) DEFAULT NULL,
  `total_bayar` int(8) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `payment`
--

INSERT INTO `payment` (`no_pemesanan`, `total_harga`, `total_kembalian`, `total_bayar`, `tgl`, `users_id`) VALUES
('ERP28072019 - 001', '93000', '7000', 100000, '2019-07-28 14:33:59', 1234),
('ERP28072019 - 002', '138500', '11500', 150000, '2019-07-28 14:39:42', 1234);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `menu_id` int(3) DEFAULT NULL,
  `qty` int(2) DEFAULT NULL,
  `meja` int(3) DEFAULT NULL,
  `no_pemesanan` varchar(20) DEFAULT NULL,
  `total_harga` int(8) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `menu_id`, `qty`, `meja`, `no_pemesanan`, `total_harga`, `tgl`, `status`, `users_id`) VALUES
(1, 8, 1, 1, 'ERP28072019 - 001', 30000, '2019-07-28 02:33:01', 1, 1234),
(2, 6, 1, 1, 'ERP28072019 - 001', 25000, '2019-07-28 02:33:01', 1, 1234),
(3, 12, 1, 1, 'ERP28072019 - 001', 38000, '2019-07-28 02:33:01', 1, 1234),
(4, 9, 1, 1, 'ERP28072019 - 002', 32500, '2019-07-28 02:39:19', 1, 1234),
(5, 12, 2, 1, 'ERP28072019 - 002', 76000, '2019-07-28 02:39:19', 1, 1234),
(6, 8, 1, 1, 'ERP28072019 - 002', 30000, '2019-07-28 02:39:19', 1, 1234);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `nip` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` int(1) DEFAULT NULL COMMENT '1: Manager, 2: Kasir, 3: Pelayan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`nip`, `nama`, `email`, `password`, `role`) VALUES
(1234, 'Kasir 1', 'Kasi1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2),
(11122, '1122', '1122@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3),
(13546, 'Kasir 2', 'kasir2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2),
(123456, 'Pelayan 1', 'plyn1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3),
(11505055, 'Manager', 'm.fauzan441@gmail.com', '1d0258c2440a8d19e716292b231e3190', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kategori_menu`
--
ALTER TABLE `kategori_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`no_pemesanan`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`nip`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kategori_menu`
--
ALTER TABLE `kategori_menu`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
