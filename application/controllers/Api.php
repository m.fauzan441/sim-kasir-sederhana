<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Api extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function login_post()
	{
		$username  = $this->post('username');
		$password  = $this->post('password');
		$cek = $this->LoginModel->auth($username,$password);
		if ($cek->num_rows() <> 0 ) {
			$data_users = $cek->row();
			$session_data = [
				'nama'=>$data_users->nama,
				'role'=>$data_users->role,
				'nip'=>$data_users->nip,
				'email'=>$data_users->email,
			];
			$data = ['status'=>true,'message'=>'Login Berhasil','userData'=>$session_data];

			if ($this->post('web')) {
				$this->session->set_userdata($session_data);
			}

		}else{
			$data = ['status'=>false,'message'=>'Username / Password Salah'];
		}
		$this->response($data,200);
	}

	public function menu_post(){
		$data = ['record'=>$this->modelMenus->getData()];
		$this->response($data,200);
	}
}

/* End of file Api.php */
/* Location: ./application/modules/login/controllers/Api.php */
 ?>