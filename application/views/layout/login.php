
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Login - SIM Kasir</title>
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/material-pro/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/material-pro/css/colors/blue.css')?>" id="theme" rel="stylesheet">
</head>

<body>
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<section id="wrapper">
    <div class="login-register" style=" background-image: radial-gradient(circle,#0b93d5,#00aff0,#0f74a8);">
        <div class="login-box card">
            <div class="card-body">
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="username" type="text" placeholder="NIP" class="form-control"   required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit"  onclick="login()">Log In</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php echo base_url('asset/material-pro/assets/plugins/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/js/jquery.slimscroll.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/js/waves.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/js/sidebarmenu.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/js/custom.min.js')?>"></script>
<script type="text/javascript">
  function login(){
    var username = $('#username').val();
    var password = $('#password').val();
    $.ajax({
              type  : 'POST',
              url   : "<?php echo site_url('api/login')?>",
              data  : {'username':username,'password':password,'web':1}, 
              success: function (result) {
                  var dataUsers = result.userData;
                  console.log(dataUsers);
                  if (result.status == false) {
                    alert(result.message);
                  }else{
                    alert(result.message);
                    switch(dataUsers.role){
                      case '3':
                        redirect = 'transaksi';
                        break;
                      case '2':
                        redirect = 'transaksi';
                        break;
                      case '1':
                        redirect = 'master/users';
                        break;
                    }
                  }
                  window.location.href=redirect;
              }
          });
  }
</script>
</body>

</html>
