
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <title>SIM Kasir Sederhana</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- chartist CSS -->
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/chartist-js/dist/chartist.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/chartist-js/dist/chartist-init.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')?>" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/c3-master/c3.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/toast-master/css/jquery.toast.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/material-pro/assets/plugins/sweetalert/sweetalert.css')?>" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('asset/material-pro/css/style.css')?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url('asset/material-pro/css/colors/purple.css')?>" id="theme" rel="stylesheet">
    <style type="text/css" media="print">
       .no-print { display: none; }
    </style>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</head>

<body class="fix-header fix-sidebar card-no-border">
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<div id="main-wrapper">
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-header">
                <a class="navbar-brand" style="color:white">
                    <b>
                        SIM
                    </b>
                    <span>
                        - Kasir
                     </span> </a>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto mt-md-0">
                    <!-- This is  -->
                    <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                    <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                </ul>
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-o"></i> <?php echo $this->session->userdata('nama')?>
                            </a>
                        <div class="dropdown-menu dropdown-menu-right scale-up">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-text">
                                            <h5><?php echo $this->session->userdata('nama')?></h5>
                                            <p class="text-muted"><?php echo $this->session->userdata('email')?></p>
                                            <p><small>NIP : <?php echo $this->session->userdata('nip')?></small></p>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo site_url('logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="left-sidebar" style="overflow: visible;">
        <div class="slimScrollDiv" style="position: relative; overflow: visible; width: auto; height: 100%;"><div class="scroll-sidebar" style="overflow: hidden; width: auto; height: 100%;">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">Menu</li>
                        <?php if ($this->session->userdata('role')==1) { ?>
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-database"></i><span class="hide-menu">Master </span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo site_url('master/users')?>">Users</a></li>
                                    <li><a href="<?php echo site_url('master/kategori-menu')?>">Kategori Menu</a></li>
                                    <li><a href="<?php echo site_url('master/menu')?>">Menu</a></li>
                                </ul>
                            </li>
                            <li> 
                                <a class="waves-effect waves-dark" href="<?php echo site_url('report')?>"><i class="fa  fa-book"></i> Report</a>
                            </li>
                        <?php }else if($this->session->userdata('role')==2 || $this->session->userdata('role')==3){ ?>
                            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Transaksi </span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo site_url('transaksi')?>">Pesan Menu</a></li>
                                    <li><a href="<?php echo site_url('transaksi/list')?>">List Pesanan</a></li>
                                </ul>
                            </li>
                        <?php } ?>

                        
                    </ul>
                </nav>
            </div><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; left: 1px; height: 446px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; left: 1px;"></div></div>
    </aside>
    <div class="page-wrapper">
        <?php $this->load->view($page); ?>
        <footer class="footer"> SIM Kasir © M. Fauzan Al hafid </footer>
    </div>
</div>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url('asset/material-pro/js/jquery.slimscroll.js')?>"></script>
<!--Wave Effects -->
<script src="<?php echo base_url('asset/material-pro/js/waves.js')?>"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url('asset/material-pro/js/sidebarmenu.js')?>"></script>
<!--stickey kit -->
<script src="<?php echo base_url('asset/material-pro/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/toast-master/js/jquery.toast.js')?>"></script>

<!--Custom JavaScript -->
<script src="<?php echo base_url('asset/material-pro/js/custom.min.js')?>"></script>
<!--c3 JavaScript -->
<script src="<?php echo base_url('asset/material-pro/assets/plugins/d3/d3.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/c3-master/c3.min.js')?>"></script>

<!-- Style switcher -->
<script src="<?php echo base_url('asset/material-pro/assets/plugins/styleswitcher/jQuery.style.switcher.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('asset/material-pro/assets/plugins/sweetalert/sweetalert.min.js')?>"></script>

<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<?php 
    $this->load->view($javascript);     
?>
</body>
</html>
