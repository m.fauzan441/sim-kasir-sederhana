<script type="text/javascript">
	var url = "<?php echo site_url('report/')?>";
	$(document).ready(function() {
        getDataTable();
    });
    
    function getDataTable(){
        var table =
            $('#tbl_pesanan').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel',
                    'print',
                    'pdf'
                ],
                ajax:({
                    url: url+'data-pesanan',
                    type: "POST",
                }),
                aaSorting: [],
                aoColumns: [
                    { data: 'no_pemesanan',title:"No Pemesanan"},
                    { data: 'tgl',title:"Tanggal"},
                    { data: 'meja',title:"Meja"},
                    { data: 'menu',title:"Menu"},
                    { data: 'qty',title:"QTY"},
                    { data: 'total_harga',title:"Total Harga"},
                    { data: 'nama',title:"Pelayan"},
                ]
            });
        $('#tbl_pesanan thead tr').clone(true).appendTo( '#tbl_pesanan thead' );
            $('#tbl_pesanan thead tr:eq(1) th').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" class="form-control" style="width:250px"/>' );
        
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        var table2 =
            $('#tbl_payment').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel',
                    'print',
                    'pdf'
                ],
                ajax:({
                    url: url+'data-payment',
                    type: "POST",
                }),
                aaSorting: [],
                aoColumns: [
                    { data: 'no_pemesanan',title:"No Pemesanan"},
                    { data: 'tgl',title:"Tanggal"},
                    { data: 'total_harga',title:"Total Harga"},
                    { data: 'total_bayar',title:"Bayar"},
                    { data: 'total_kembalian',title:"Total Kembalian"},
                    { data: 'nama',title:"Kasir"},
                ]
            });
        $('#tbl_payment thead tr').clone(true).appendTo( '#tbl_payment thead' );
            $('#tbl_payment thead tr:eq(1) th').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" class="form-control" style="width:250px"/>' );
        
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    }
</script>