<style type="text/css" media="screen">
	table.dataTable thead .sorting_asc:nth-child(1):after{
        content:''
    }

    table.dataTable thead .sorting:after{
        content:''
    }
</style>
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Report</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Report </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Report </h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills m-t-30 m-b-30">
                        <li class="nav-item"><a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">Report Pemesanan</a> </li>
                        <li class="nav-item"><a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false" >Report Pembayaran</a> </li>
                    </ul>
                    <div class="tab-content ">
                        <div id="navpills-1" class="tab-pane active">
                            <div class="col-md-12">
                                <table id="tbl_pesanan" class="table display table-responsive table-bordered table-striped"></table>
                            </div>
                        </div>
                        <div id="navpills-2" class="tab-pane">
                            <div class="col-md-12">
                            	<table id="tbl_payment" class="table display table-responsive table-bordered table-striped" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>