<?php 
 defined('BASEPATH') OR exit('No direct script access allowed');
 
 class Report extends MX_Controller {
 	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('nip') OR $this->session->userdata('role')!=1) {
			echo "<script>alert('Silahkan Login Terlebih Dahulu')</script>";
			redirect('/login','refresh');
		}
	}
 	public function index()
 	{
 		$data = [
			'page'=>'report/index',
			'javascript'=>'report/index_js'
		];
		$this->load->view('layout/template',$data);	
 	}

 	public function getDataReportPayment(){
 		$response['data']=$this->TransaksiModel->getDataPayment();
		echo json_encode($response);
 	}

 	public function getDataReportPesanan(){
 		$response['data']=$this->TransaksiModel->getPesanan();
		echo json_encode($response);
 	}
 
 }
 
 /* End of file Report.php */
 /* Location: ./application/modules/report/controllers/Report.php */ ?>