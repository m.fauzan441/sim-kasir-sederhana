<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiModel extends CI_Model {


	function savePesanan($meja,$field,$updated=false){
		if ($updated) {
			$this->db->where('meja', $meja)->where('transaksi.status is null')->delete('transaksi');
			$this->db->insert_batch('transaksi', $field);
		}else{
			$this->db->insert_batch('transaksi', $field);
		}
		return $this->db->affected_rows();

	}
	function getCategoryMenu(){
		return $this->db->order_by('kategori','asc')->get('kategori_menu')->result();
	}

	function getMenuByCategory($id){
		return $this->db->where('kategori_menu_id',$id)->order_by('menu','asc')->get('menu')->result();
	}

	function getLastNoPesanan($meja){
		$checkmeja = $this->db->where('meja', $meja)->where('status is null')->get('transaksi');
		if ($checkmeja->num_rows() <> 0 ) {
			$noPesanan = $checkmeja->row()->no_pemesanan;
		}else{
			$getLast =  $this->db->select_max('no_pemesanan')->get('transaksi')->row()->no_pemesanan;
			if ($getLast) {
				$exp = explode(' - ',$getLast);
				$noPesanan =  "ERP".date('dmY').' - '.str_pad($exp[1]+=1, 3, '0', STR_PAD_LEFT);	
			}else{
				$noPesanan =  "ERP".date('dmY').' - '.str_pad(1, 3, '0', STR_PAD_LEFT);	
			}
		}
		return $noPesanan;
	}

	function getPesananByMeja($nomeja,$all=false){
		if ($all) {
			$data =  $this->db->select('transaksi.*, menu.harga, menu.menu')
				->join('menu','transaksi.menu_id=menu.id')
				->where('meja',$nomeja)
				->get('transaksi')->result();
		}else{
			$data =  $this->db->select('transaksi.*, menu.harga, menu.menu')
				->join('menu','transaksi.menu_id=menu.id')
				->where('meja',$nomeja)
				->where('transaksi.status is null')
				->get('transaksi')->result();
		}
		return $data;
		
	}

	function getMejaActive(){
		if ($this->session->userdata('role')==3) {
			return $this->db->select('meja')->where('transaksi.status is null')->where('users_id',$this->session->userdata('nip'))->group_by('meja')->get('transaksi')->result();
		}else{
			return $this->db->select('meja')->where('transaksi.status is null')->group_by('meja')->get('transaksi')->result();
		}
	}

	function lastTransaksi($no_pemesanan,$field){
		$this->db->where('no_pemesanan', $no_pemesanan)->update('transaksi',['status'=>1]);
		$this->db->insert('payment',$field);
		return $this->db->affected_rows();
	}

	function getPaymentByNoPesanan($no_pemesanan){
		return $this->db
		->select('payment.*,users.nama')
		->where('no_pemesanan',$no_pemesanan)
		->join('users','users.nip=payment.users_id')
		->get('payment')
		->row();
	}

	function getDataPayment(){
		return $this->db
		->select('payment.*,users.nama')
		->join('users','users.nip=payment.users_id')
		->order_by('no_pemesanan','desc')
		->get('payment')
		->result();
	}

	function getPesanan(){
		$data =  $this->db->select('transaksi.*, menu.harga, menu.menu,kategori_menu.kategori,users.nama')
			->order_by('no_pemesanan','desc')
			->where('transaksi.status is not null')
			->join('menu','transaksi.menu_id=menu.id')
			->join('kategori_menu','kategori_menu.id=menu.kategori_menu_id')
			->join('users','users.nip=transaksi.users_id')
			->get('transaksi')->result();
		return $data;
		
	}

}

/* End of file TransaksiModel.php */
/* Location: ./application/modules/transaksi/models/TransaksiModel.php */
 ?>