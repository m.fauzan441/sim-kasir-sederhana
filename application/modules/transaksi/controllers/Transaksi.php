<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('nip')) {
			echo "<script>alert('Silahkan Login Terlebih Dahulu')</script>";
			redirect('/login','refresh');
		}
	}
	public function index()
	{
		$data = [
			'page'=>'transaksi/index',
			'dataKategori'=>$this->modelMenus->getDataKategoriMenu(),
			'javascript'=>'transaksi/js'
		];
		$this->load->view('layout/template',$data);		
	}

	public function getDataMenu(){
		$id = $this->input->post('id');
		$response['data']= $this->modelMenus->getByCategory($id);
		echo json_encode($response);
	}

	public function Save(){
		$data = $this->input->post('data');
		$meja = $this->input->post('noMeja');
		$updated = false;

		$logPesananMenu = '';
		
		foreach ($data as $key => $value) {
			$field[] = [
				'menu_id'=>$value['menu_id'],
				'qty'=>$value['qty'],
				'total_harga'=>$value['total_harga'],
				'tgl'=>date('Y-m-d h:i:s'),
				'meja'=>$meja,
				'users_id'=>$this->session->userdata('nip'),
				'no_pemesanan'=> ( $value['no_pemesanan'] ? $value['no_pemesanan'] : $this->TransaksiModel->getLastNoPesanan($meja))
			];
			if ($value['no_pemesanan']) {
				$updated = true;
				$no_pemesanan = $value['no_pemesanan'];
				
			}

			$logPesananMenu.=" Menu : ".$value['menu']." | qty : ".$value['qty']." | total harga : ". $value['total_harga'];
			$logPesananMenu.="\n";

		}
		log_message('info',  $this->session->userdata('nama').' melakukan '.($updated==true?'perubahan pada No Pemesanan '.$no_pemesanan:'pesanan baru'). " dengan pesanan \n" .$logPesananMenu); 
		if ($this->TransaksiModel->savePesanan($meja,$field,$updated)) {
			$response['status']=1;
		}else{
			$response['status']=0;
		}
		echo json_encode($response);
	}

	public function getDataLastPesanan(){
		$meja = $this->input->post('no_meja');
		$response['data']=$this->TransaksiModel->getPesananByMeja($meja);
		echo json_encode($response);
	}

	public function list(){
		$data = [
			'page'=>'transaksi/listPesanan',
			'listMeja'=>$this->TransaksiModel->getMejaActive(),
			'javascript'=>'transaksi/js'
		];
		$this->load->view('layout/template',$data);		
	}


	public function payment(){

		$no_pemesanan = $this->input->post('no_pemesanan');
		$total = $this->input->post('total');
		$bayar = $this->input->post('bayar');
		$kembalian = $this->input->post('kembalian');
		$fieldPayment = [
			'no_pemesanan'=>$no_pemesanan,
			'total_harga'=>$total,
			'total_kembalian'=>$kembalian,
			'total_bayar'=>$bayar,
			'tgl'=>date('Y-m-d H:i:s')
		];
		if ($this->TransaksiModel->lastTransaksi($no_pemesanan,$fieldPayment)) {
			$response['status']=1;
		}else{
			$response['status']=0;
		}
		log_message('info',$this->session->userdata('nama'). ' melakukan pembayaran dengan No Pemesanan : '.$no_pemesanan." \n Total Harga : ".$total." | Total Bayar : ". $bayar." | Total Kembalian : ".$total_kembalian);
		echo json_encode($response);
	}

	public function printout($meja){
		$data['listPesanan']=$this->TransaksiModel->getPesananByMeja($meja,true);
		$data['detailPembayaran']=$this->TransaksiModel->getPaymentByNoPesanan($data['listPesanan'][0]->no_pemesanan);
		$this->load->view('transaksi/print_struk',$data);	
	}


}

/* End of file Transaksi.php */
/* Location: ./application/modules/transaksi/controllers/Transaksi.php */
 ?>