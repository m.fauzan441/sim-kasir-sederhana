<div class="container-fluid no-print">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">List Pesan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Transaksi </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">List Pesan</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card card-outline-info form-horizontal">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">List Pemesanan Per-Meja</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <select onchange="editData(this.value)" class="form-control col-md-4" id="noMeja">
                            <option value="">Pilih Meja</option>
                            <?php foreach ($listMeja as $r) {?>
                                <option value="<?php echo $r->meja ?>"><?php echo $r->meja ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <table class="table display table-bordered table-striped" id="TablePesanan">
                    </table>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-danger waves-effect waves-light" onclick="saveData()">Update</button>
                        <?php if ($this->session->userdata('role')==2){?>
                            <button type="submit" class="btn btn-success waves-effect waves-light" onclick="Pay()">Bayar</button>
                        <?php  }?>

                    </div>
                </div>
            </div>
        </div>
        <?php if ($this->session->userdata('role')==2){?>
        <div class="col-md-4">
            <div class="card card-outline-info form-horizontal">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Pembayaran</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label">No Meja :</label>
                        <input type="text" class="form-control" id="no_meja" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Total Harga :</label>
                        <input type="text" class="form-control" id="total_harga_akhir" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Total Bayar :</label>
                        <input type="text" class="form-control" id="total_bayar" onchange="getKembalian()">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Total Kembalian :</label>
                        <input type="text" class="form-control" id="total_kembalian" readonly>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-danger waves-effect waves-light" onclick="payment()">Selesai</button>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>