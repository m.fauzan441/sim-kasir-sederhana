<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Menu Pesan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Transaksi </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Menu Pesan</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">List Menu</h4>
                </div>
                <div class="card-body">
                    <div class="vtabs">
                        <ul class="nav nav-tabs tabs-vertical" role="tablist">
                            <?php 
                            foreach ($dataKategori as $key) {
                             ?>
                            <li class="nav-item"> 
                                <a class="nav-link" data-toggle="tab" onclick="getDataMenu('<?php echo $key->id?>')" role="tab" aria-expanded="false">
                                    <span class="hidden-sm-up"><i class="ti-home"></i></span> 
                                    <span class="hidden-xs-down"><?php echo $key->kategori?></span> 
                                </a> 
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content  col-md-12">
                             <table id="Table" class="table display table-bordered table-striped"></table>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-outline-info form-horizontal">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">List Pemesanan | 
                        <b><a title="Simpan" onclick="getAllDataPesanan()"><i class="fa  fa-save  waves-effect waves-light "></i></a><b>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label">No Meja :</label>
                        <input type="text" class="form-control col-md-2" id="noMeja">
                    </div>
                    <table class="table display table-bordered table-striped" id="TablePesanan">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Konfirmasi List Pesanan</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Menu</th>
                            <th>QTY</th>
                            <th>Total Harga</th>
                        </tr>
                    </thead>
                    <tbody id="tableConfPesanan"></tbody>
                </table>
                <p id="TotalHargaKonfirmasi"> Total Harga : </p>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-danger waves-effect waves-light" onclick="saveData()">Save</button>
                </div>
        </div>
    </div>
</div>
</div>