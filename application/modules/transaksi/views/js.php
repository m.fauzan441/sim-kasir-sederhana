<script src="https://cdn.jsdelivr.net/npm/jquery.appendgrid@2.0.2/dist/AppendGrid.js"></script>
<script type="text/javascript">
    var myAppendGrid = new AppendGrid({
		  element: "TablePesanan",
		  uiFramework: "bootstrap4",
		  columns: [
		  	{
		      name: "menu_id",
		      display: "menu_id",
		      type:'hidden',
	          ctrlAttr: {'readonly' :true }
		    },
		    {
		      name: "no_pemesanan",
		      display: "no_pemesanan",
		      type:'hidden',
	          ctrlAttr: {'readonly' :true }
		    },
		    {
		      name: "harga",
		      display: "harga",
		      type:'hidden',
	          ctrlAttr: {'readonly' :true }
		    },
		    {
		      name: "menu",
		      display: "Menu",
	          ctrlAttr: {'readonly' :true }
		    },
		    {
		      name: "qty",
		      display: "QTY",
		      events: {
		        input: function(e) {
		          TotalHarga(e.uniqueIndex);
		        }
		      }
		    },
		    {
		      name: "total_harga",
		      display: "Total Harga",
	          ctrlAttr: {'readonly' :true }
		    },
		  ],
		  initRows:0,
		  hideButtons: {
		        moveUp: true,
		        moveDown: true,
		        removeLast:true,
		        append:true,
		        insert	:true
		    },
		});
	
    function getDataMenu(id){
    	var table =
            $('#Table').DataTable({
                ajax:({
                    url: "<?php echo site_url('transaksi/getDataMenu')?>",
                    type: "POST",
                    data:{id}
                }),
                columnDefs: [
                    { orderable: false, targets: [0,3] },
                ],
                aoColumns: [
                	{
                        mData: "id",
                        mRender: function (data, type, row) {
                            if (row.status == 1) {
                            	return '<a title="Tambahkan" onclick="selectedMenu('+data+',\''+row.menu+'\',\''+row.harga+'\')"><i class="fa  fa-plus-square btn waves-effect waves-light btn-primary"></i></a>';

                            }else{
                            	return '<i class="fa fa-close btn waves-effect waves-light btn-danger" ></i>';
                            }
                        }
                    },
                    { data: 'menu',title:"Menu"},
                    { data: 'harga',title:"Harga"},
                    {
                        mData: "picture",
                        mRender: function (data, type, row) {
                            return '<img src="<?php echo base_url('asset/img/')?>'+data+'" alt="image" class="img-responsive img-thumbnail" >';
                        },
                        title:"Foto",
                        width:'200'
                    },
                     
                ],
                destroy:true
            });

      
    }

    function TotalHarga(uniqueIndex) {
	  var rowIndex = myAppendGrid.getRowIndex(uniqueIndex);
	  var number1 = parseFloat(myAppendGrid.getCtrlValue("harga", rowIndex) || 0);
	  var number2 = parseFloat(myAppendGrid.getCtrlValue("qty", rowIndex) || 0);
	  var total = number1 * number2;
	  myAppendGrid.setCtrlValue("total_harga", rowIndex, total);
	}

	function selectedMenu(id,menu,harga){
		 myAppendGrid.appendRow([ 
		    { 'menu_id':id,"menu": menu,"harga":harga}
		]);

	}

	function getAllDataPesanan(){
		var data = myAppendGrid.getAllValue();
		var html = '';
		var j = 0;
		var totalHarga = 0;
		for (var i = 0; i < data.length; i++) {
			 html+='<tr>'+
				 		'<td>'+ (j+=1) +'</td>'+
				 		'<td>'+data[i].menu+'</td>'+
				 		'<td>'+data[i].qty+'</td>'+
				 		'<td>'+data[i].total_harga+'</td>'+
				 '</tr>';
			totalHarga+=parseFloat(data[i].total_harga);
		}
		
		$('#modal').modal();
		$('#TotalHargaKonfirmasi').html("Total Pesanan : " + totalHarga);
		$('#tableConfPesanan').html(html);
	}

	function saveData(){
		var noMeja = $('#noMeja').val();
		$.ajax({
            type:'POST',
            url:"<?php echo site_url('transaksi/Save') ?>",
            data:{
                data:myAppendGrid.getAllValue(),
                'noMeja':noMeja
            },
            success:function(data){
            	var response = $.parseJSON(data);
            	if (response.status == 1) {
            		alert('Pesanan Berhasil Dipesan');
            		location.reload();
            	}else{
            		alert('Pesanan Gagal Dipesan');
            	}
            }
        });
	}

	function editData(no_meja){
		$.ajax({
            type:'POST',
            url:"<?php echo site_url('transaksi/getDataLastPesanan') ?>",
            data:{no_meja},
            success:function(data){
            	var valData = $.parseJSON(data);
            	myAppendGrid.load(valData.data);
            }
        });
	}

	function Pay(){
		$('#no_meja').val($('#noMeja').val());
		var data = myAppendGrid.getAllValue();
		var totalHarga = 0;
		for (var i = 0; i < data.length; i++) {
			totalHarga+=parseFloat(data[i].total_harga);
		}
		$('#total_harga_akhir').val(totalHarga);
	}

	function getKembalian(){
		var bayar = $('#total_bayar').val();
		var total = $('#total_harga_akhir').val();
		var kembalian  = parseFloat(bayar) - parseFloat(total);
		$('#total_kembalian').val(kembalian);

	}

	function payment(){
		var no_pemesanan = myAppendGrid.getCtrlValue("no_pemesanan", 0);
		var bayar = $('#total_bayar').val();
		var total = $('#total_harga_akhir').val();
		var kembalian  = $('#total_kembalian').val();

		$.ajax({
            type:'POST',
            url:"<?php echo site_url('transaksi/payment') ?>",
            data:{no_pemesanan,bayar,total,kembalian},
            success:function(data){
            	window.open("<?php echo site_url('transaksi/printout/') ?>"+$('#no_meja').val(),'_blank');
            	location.reload();
            }
        });
	}


</script>