<!DOCTYPE html>
<html>
<head>
	<title>Print</title>
	<link href="<?php echo base_url('asset/material-pro/assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
</head>
<body onload="window.print()" style="margin:3cm">
<div style="width: 100%">
	<table width="60%">
		<tr>
			<td style="border: none">No Pemesanan </td>
			<td style="border: none">: <?php echo $detailPembayaran->no_pemesanan ?></td>
		</tr>
		<tr>
			<td style="border: none">Kasir </td>
			<td style="border: none">: <?php echo $detailPembayaran->nama ?></td>
		</tr>
		<tr>
			<td style="border: none">Tanggal </td>
			<td style="border: none">: <?php echo $detailPembayaran->tgl ?></td>
		</tr>
	</table>
	<table class="table table-border">
		<thead>
			<tr>
				<th>No</th>
				<th>Menu</th>
				<th>QTY</th>
				<th>Harga</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; foreach($listPesanan as $r){ ?>
				<tr>
					<td><?php echo $i ?></td>
					<td><?php echo $r->menu ?></td>
					<td><?php echo $r->qty ?></td>
					<td><?php echo $r->total_harga ?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	<p style="text-align: right">Total : <?php echo $detailPembayaran->total_harga ?></p>
	<p style="text-align: right">Pembayaran : <?php echo $detailPembayaran->total_bayar ?></p>
	<p style="text-align: right">Kembalian : <?php echo $detailPembayaran->total_kembalian ?></p>
</div>
</body>
</html>