<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login/LoginModel');
	}
	public function index()
	{
		$data['javascript']="login/js";
		$this->load->view('layout/login',$data);		
	}

	public function logout(){
		session_destroy();
		redirect('/login','refresh');
	}

}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */
 ?>
