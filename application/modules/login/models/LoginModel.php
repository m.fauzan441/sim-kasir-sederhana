<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

	function auth($username,$password){
		$cek = $this->db->where(
			[
				'nip'=>$username,
				'password'=>md5($password)
			]
		)->get('users');

		return $cek ;
	}

	function updateUsers($nip,$field){
		$this->db->where('nip',$nip)->update('users',$field);
	}

	function checkToken($token){
		return $this->db->where('token',$token)->get('users')->num_rows();
	}

}

/* End of file LoginModel.php */
/* Location: ./application/modules/login/models/LoginModel.php */
 ?>