<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Master Users</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Master </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Users</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">List Users <i class="fa fa-refresh" id="reload"></i></h4>
                </div>
                <div class="card-body">
                    <table id="Table" class="table display table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>E-mail</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>E-mail</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Form Users</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">NIP :</label>
                        <input type="text" class="form-control" id="nip" required>
                        <input type="hidden" id="oldNip">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama :</label>
                        <input type="text" class="form-control" id="nama" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">E-mail :</label>
                        <input id="email" type="email" class="form-control" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Role : </label>
                        <select class="form-control" id="role">
                            <option value="">Select Role</option>
                            <option value="1">Manager</option>
                            <option value="2">Kasir</option>
                            <option value="3">Pelayan</option>
                        </select>
                    </div>
                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" value="Save" onclick="save()" id="save">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>