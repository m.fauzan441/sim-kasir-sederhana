<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Master Menu</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Master </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Menu</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">List Menu <i class="fa fa-refresh" id="reload"></i> | <i class="fa fa-plus" onclick="openModalSave()"></i></h4>
                </div>
                <div class="card-body">
                    <table id="Table" class="table display table-bordered table-striped">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-save" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Form Menus</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('master/menu-data') ?>" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="case" id="case" value="save">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Kategori Menu :</label>
                        <select name="kategori" id="kategori" class="form-control"></select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Menu:</label>
                        <input type="text" class="form-control" name="menu" id="menu">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Harga:</label>
                        <input type="text" class="form-control" name="harga" id="harga">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Foto:</label>
                        <input type="file" name="foto" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Status Menu :</label>
                        <select name="status" id="status" class="form-control">
                            <option value="1">Ready</option>
                            <option value="0">Tidak Ready</option>
                        </select>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>