<script type="text/javascript">
	var url = "<?php echo site_url('master/menu-data')?>";
	$(document).ready(function() {
        getDataTable();
        document.getElementById('reload').onclick = function(){
             $('#Table').dataTable()._fnAjaxUpdate();
        }
    });
    
    function getDataTable(){

        var table =
            $('#Table').DataTable({
                ajax:({
                    url: url,
                    type: "POST",
                    data:{'case':'getData'}
                }),
                columnDefs: [
                    { orderable: false, targets: [4,5] },
                ],
                aoColumns: [
                    { data: 'kategori',title:"Kategori Menu"},
                    { data: 'menu',title:"Menu"},
                    { data: 'harga',title:"Harga"},
                    {
                        mData: "status",
                        mRender: function (data, type, row) {
                            return data == 1 ? 'Ready' : '-';
                        },
                        title:"Status"
                    },
                    {
                        mData: "picture",
                        mRender: function (data, type, row) {
                            return '<img src="<?php echo base_url('asset/img/')?>'+data+'" alt="image" class="img-responsive img-thumbnail" >';
                        },
                        title:"Foto",
                        width:'200'
                    },
                     {
                        mData: "id",
                        mRender: function (data, type, row) {
                            return "<a title='Edit' onclick=edit("+data+")><i class='fa fa-pencil btn waves-effect waves-light btn-warning'></i></a> | <a title='Hapus' onclick=deleted("+data+")><i class='btn waves-effect waves-light btn-danger fa fa-trash'></i></a>"
                        }
                    },
                ]
            });
    }
    function openModalSave(id=false){
        $.ajax({
                type:'POST',
                url:"<?php echo site_url('master/kategori-menu-data')?>",
                data:{
                    case:'getData'
                },
                success:function(data){
                    var parse = $.parseJSON(data);
                    var datas = parse.data;
                    var opt = '';
                    for (var i = 0;i<datas.length;i++) {
                        opt+='<option value="'+datas[i].id+'">'+datas[i].kategori+'</option>'
                    }
                    document.getElementById('kategori').innerHTML=opt;
                    if (id) {
                        $('#kategori').val(id);    
                    }
                    $('#modal-save').modal();

                }
            });
    }

    function edit(id){
        $.ajax({
                type:'POST',
                url:url,
                data:{
                    case:'edit',id
                },
                success:function(data){
                     
                     valData = $.parseJSON(data);
                     openModalSave(valData.kategori_menu_id);
                     $('#menu').val(valData.menu);
                     $('#harga').val(valData.harga);
                     $('#status').val(valData.status);
                     $('#id').val(valData.id);
                     $('#case').val('update');
                }
            });

    }
	
    function deleted(id){
            swal({
                title: "Data akan dihapus?",
                text: "Data Menu Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{
                            id:id,
                            case:'delete'
                        },
                        success:function(data){
                            swal("Deleted!", "Data Berhasil dihapus.", "success");
                            $('#Table').dataTable()._fnAjaxUpdate();
                        }
                    });
                    
                }else {
                    swal("Cancelled", "", "error");
                }
            });
    }
</script>