<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor">Master Kategori Menu</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Master </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Kategori Menu</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">List Kategori Menu <i class="fa fa-refresh" id="reload"></i></h4>
                </div>
                <div class="card-body">
                    <table id="Table" class="table display table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kategori</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Kategori</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Form Kategori Menu</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Kategori :</label>
                        <input type="text" class="form-control" id="kategori" required>
                        <input type="hidden" id="id">
                    </div>
                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" value="Save" onclick="save()" id="save">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>