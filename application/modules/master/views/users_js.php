<script type="text/javascript">
	var url = "<?php echo site_url('master/users_data')?>";
	$(document).ready(function() {
        getDataTable();
        document.getElementById('reload').onclick = function(){
             $('#Table').dataTable()._fnAjaxUpdate();
        }
    });
	function save(){
		$.ajax({
                type:'POST',
                url:url,
                data:{
                    nama: $('#nama').val(),
                    role: $('#role').val(),
                    email: $('#email').val(),
                    nip: $('#nip').val(),
                    case:'save'
                },
                success:function(data){
                    $.toast({
                        heading: 'Info',
                        text: "Data Berhasil Disimpan",
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500,
                        stack: 6
                    });
                    clearTextBox();
                    $('#Table').dataTable()._fnAjaxUpdate();
                }
            });
	}
    function getDataTable(){
        var table =
            $('#Table').DataTable({
                ajax:({
                    url: url,
                    type: "POST",
                    data:{'case':'getData'}
                }),
                "columnDefs": [
                    { "orderable": false, "targets": 4 },
                ],
                aoColumns: [
                    { data: 'nip'},
                    { data: 'nama'},
                    { data: 'email'},
                    {
                        "mData": "role",
                        "mRender": function (data, type, row) {
                        	var roles;
                            if (data==1) {
                            	roles="Manager";
                            }else if (data==2) {
                            	roles="Kasir";
                            }else if (data==3) {
                            	roles="Pelayan";
                            }

                            return roles;
                        }
                    },
                    {
                        "mData": "nip",
                        "mRender": function (data, type, row) {
                            return "<a title='Edit' onclick=edit("+data+")><i class='fa fa-pencil btn waves-effect waves-light btn-warning'></i></a> | <a title='Hapus' onclick=deleted("+data+")><i class='btn waves-effect waves-light btn-danger fa fa-trash'></i></a>"
                        }
                    },
                ]
            });
    }

   function edit(nip){
        $.ajax({
            url: url,
            type: "POST",
            data:{'case':'edit','nip':nip},
            success:function(data){
                valData = $.parseJSON(data);
                $('#nama').val(valData.nama);
                $('#role').val(valData.role);
                $('#email').val(valData.email);
                $('#nip').val(valData.nip);
                $('#oldNip').val(valData.nip);
            }
        });
        document.getElementById('save').setAttribute("onclick","update()");
        document.getElementById('save').onclick = function()
        {
            $.ajax({
                type:'POST',
                url:url,
                data:{
                    nama: $('#nama').val(),
                    role: $('#role').val(),
                    email: $('#email').val(),
                    nip: $('#nip').val(),
                    oldNip :$('#oldNip').val(),
                    case:'update'
                },
                success:function(data){
                    $.toast({
                        heading: 'Info',
                        text: "Data Berhasil Diupdate",
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500,
                        stack: 6
                    });
                    clearTextBox();
                    $('#Table').dataTable()._fnAjaxUpdate();
                    document.getElementById('save').setAttribute("onclick","save()");
                }
            });
        };
    }

    function deleted(nip){
            swal({
                title: "Data akan dihapus?",
                text: "Data Users Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{
                            nip:nip,
                            case:'delete'
                        },
                        success:function(data){
                            swal("Deleted!", "Data Berhasil dihapus.", "success");
                            $('#Table').dataTable()._fnAjaxUpdate();
                        }
                    });
                    
                }else {
                    swal("Cancelled", "", "error");
                }
            });
    }
    function clearTextBox(){
		$('#nama').val('');
		$('#role').val('');
		$('#email').val('');
		$('#nip').val('');
    }
</script>