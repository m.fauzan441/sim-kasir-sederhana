<script type="text/javascript">
	var url = "<?php echo site_url('master/kategori-menu-data')?>";
	$(document).ready(function() {
        getDataTable();
        document.getElementById('reload').onclick = function(){
             $('#Table').dataTable()._fnAjaxUpdate();
        }
    });
	function save(){
		$.ajax({
                type:'POST',
                url:url,
                data:{
                    kategori: $('#kategori').val(),
                    case:'save'
                },
                success:function(data){
                    $.toast({
                        heading: 'Info',
                        text: "Data Berhasil Disimpan",
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500,
                        stack: 6
                    });
                    clearTextBox();
                    $('#Table').dataTable()._fnAjaxUpdate();
                }
            });
	}
    function getDataTable(){
        var table =
            $('#Table').DataTable({
                ajax:({
                    url: url,
                    type: "POST",
                    data:{'case':'getData'}
                }),
                "columnDefs": [
                    { "orderable": false, "targets": 1 },
                ],
                aoColumns: [
                    { data: 'kategori'},
                    {
                        "mData": "id",
                        "mRender": function (data, type, row) {
                            return "<a title='Edit' onclick=edit("+data+")><i class='fa fa-pencil btn waves-effect waves-light btn-warning'></i></a> | <a title='Hapus' onclick=deleted("+data+")><i class='btn waves-effect waves-light btn-danger fa fa-trash'></i></a>"
                        }
                    },
                ]
            });
    }

   function edit(id){
        $.ajax({
            url: url,
            type: "POST",
            data:{'case':'edit','id':id},
            success:function(data){
                valData = $.parseJSON(data);
                $('#id').val(valData.id);
                $('#kategori').val(valData.kategori);
            }
        });
        document.getElementById('save').setAttribute("onclick","update()");
        document.getElementById('save').onclick = function()
        {
            $.ajax({
                type:'POST',
                url:url,
                data:{
                    kategori: $('#kategori').val(),
                    id :$('#id').val(),
                    case:'update'
                },
                success:function(data){
                    $.toast({
                        heading: 'Info',
                        text: "Data Berhasil Diupdate",
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500,
                        stack: 6
                    });
                    clearTextBox();
                    $('#Table').dataTable()._fnAjaxUpdate();
                    document.getElementById('save').setAttribute("onclick","save()");
                }
            });
        };
    }

    function deleted(id){
            swal({
                title: "Data akan dihapus?",
                text: "Data Kategori Menu Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        type:'POST',
                        url:url,
                        data:{
                            id:id,
                            case:'delete'
                        },
                        success:function(data){
                            swal("Deleted!", "Data Berhasil dihapus.", "success");
                            $('#Table').dataTable()._fnAjaxUpdate();
                        }
                    });
                    
                }else {
                    swal("Cancelled", "", "error");
                }
            });
    }
    function clearTextBox(){
		$('#kategori').val('');
		$('#id').val('');
    }
</script>