<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends MX_Controller {
	 public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('nip')) {
			echo "<script>alert('Silahkan Login Terlebih Dahulu')</script>";
			redirect('/login','refresh');
		}else if($this->session->userdata('role') != 1) {
			echo "<script>alert('Anda Tidak Diberi Akses Menu Ini!')</script>";
			redirect('/logout','refresh');
		}
	}
	public function users(){
		$data = [
			'page'=>'master/users',
			'javascript'=>'master/users_js'
		];
		$this->load->view('layout/template', $data);				
	}

	public function users_data(){
		switch ($this->input->post('case')) {
			case 'getData':
				$response['data']=$this->modelUsers->getData();
			break;

			case 'edit':
				$nip = $this->input->post('nip');
				$response = $this->modelUsers->getDataByNip($nip);
			break;

			case 'save':
				$nip = $this->input->post('nip');
				$nama = $this->input->post('nama');
				$email = $this->input->post('email');
				$role = $this->input->post('role');
				$field = [
					'nip' => $nip,
					'nama' => $nama,
					'email' => $email,
					'role' => $role,
					'password' => md5('123456'),
				];
				$insert = $this->modelUsers->insert($field);
				if ($insert) {
					$response['message']="Data Berhasil Disimpan";
				}
			break;

			case 'update':
				$oldNip = $this->input->post('oldNip');
				$nip = $this->input->post('nip');
				$nama = $this->input->post('nama');
				$email = $this->input->post('email');
				$role = $this->input->post('role');
				$field = [
					'nip' => $nip,
					'nama' => $nama,
					'email' => $email,
					'role' => $role,
				];
				$update = $this->modelUsers->update($oldNip,$field);
				$response['status']=1;
			break;

			case 'delete':
				$nip = $this->input->post('nip');
				$delete = $this->modelUsers->delete($nip);
				$response['status']=1;
			break;
		}
		echo json_encode($response);
	}

	public function kategoriMenu(){
		$data = [
			'page'=>'master/kategori_menu',
			'javascript'=>'master/kategori_menu_js'
		];
		$this->load->view('layout/template', $data);				
	}

	public function kategoriMenuData(){
		switch ($this->input->post('case')) {
			case 'getData':
				$response['data']=$this->modelMenus->getDataKategoriMenu();
			break;

			case 'edit':
				$id = $this->input->post('id');
				$response = $this->modelMenus->getDataKategoriMenuById($id);
			break;

			case 'save':
				$kategori = $this->input->post('kategori');
				$field = [
					'kategori' => $kategori,
				];
				$insert = $this->modelMenus->insertKategoriMenu($field);
				if ($insert) {
					$response['message']="Data Berhasil Disimpan";
				}
			break;

			case 'update':
				$id = $this->input->post('id');
				$kategori = $this->input->post('kategori');
				$field = [
					'kategori' => $kategori,
				];
				$update = $this->modelMenus->updateKategoriMenu($id,$field);
				$response['status']=1;
			break;

			case 'delete':
				$id = $this->input->post('id');
				$delete = $this->modelMenus->deleteKategoriMenu($id);
				$response['status']=1;

			break;
		}
		echo json_encode($response);
	}

	public function menu(){
		$data = [
			'page'=>'master/makanan',
			'javascript'=>'master/makanan_js'
		];
		$this->load->view('layout/template', $data);	
	}

	public function MenuData(){
		switch ($this->input->post('case')) {
			case 'getData':
				$response['data']=$this->modelMenus->getData();
				echo json_encode($response);
			break;
			case 'edit':
				$id = $this->input->post('id');
				$response = $this->modelMenus->getDataById($id);
				echo json_encode($response);
			break;

			case 'save':
				$kategori = $this->input->post('kategori');
				$menu = $this->input->post('menu');
				$status = $this->input->post('status');
				$harga = $this->input->post('harga');
				$field = [
					'kategori_menu_id' => $kategori,
					'menu' => $menu,
					'status' => $status,
					'harga' => $harga,
				];
				$foto = $this->uploadFile('foto');
				if ($this->uploadFile('foto')!='') {

					$field['picture']=$foto;
				}
				$update = $this->modelMenus->insert($field);
				echo "<script>alert('Data Berhasil Disimpan')</script>";
				redirect('/master/menu','refresh');
			break;
			case 'update':
				$id = $this->input->post('id');
				$kategori = $this->input->post('kategori');
				$menu = $this->input->post('menu');
				$status = $this->input->post('status');
				$harga = $this->input->post('harga');
				$field = [
					'kategori_menu_id' => $kategori,
					'menu' => $menu,
					'status' => $status,
					'harga' => $harga,
				];
				$foto = $this->uploadFile('foto');
				if ($this->uploadFile('foto')!='') {
					$field['picture']=$foto;
				}
				$update = $this->modelMenus->update($id,$field);
				echo "<script>alert('Data Berhasil Diupdate')</script>";
				redirect('/master/menu','refresh');
			break;

			case 'delete':
				$id = $this->input->post('id');
				$delete = $this->modelMenus->delete($id);
				$response['status']= 1;
				echo json_encode($response);
			break;

			
		}
		
	}

	public function uploadFile($textboxt){
		$config['upload_path'] = './asset/img';
		$config['allowed_types'] = 'jpeg|jpg|png';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($textboxt)){
			$fileNames="";
		}else{
			$fileNames = $this->upload->data('file_name');
		}
		return $fileNames;
	}

}

/* End of file Users.php */
/* Location: ./application/modules/master/controllers/Users.php */
 ?>