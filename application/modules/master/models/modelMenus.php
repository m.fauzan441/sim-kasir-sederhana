<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelMenus extends CI_Model {

	function getData(){
		return $this->db->select('menu.*,kategori_menu.kategori')
		->join('kategori_menu','menu.kategori_menu_id=kategori_menu.id')
		->order_by('kategori','asc')
		->order_by('status','desc')
		->order_by('menu','asc')
		->get('menu')->result();
	}

	function getDataById($id){
		return $this->db->where('id',$id)->get('menu')->row();	
	}
	function insert($field){
		$this->db->insert('menu',$field);
	}

	function update($id,$field){
		$this->db->where('id',$id)->update('menu',$field);
	}

	function delete($id){
		$this->db->where('id', $id)->delete('menu');
	}

	function getByCategory($id){
		return $this->db->where('kategori_menu_id',$id)
		->order_by('status','desc')
		->order_by('menu','asc')
		->get('menu')->result();
	}



	function getDataKategoriMenu(){
		return $this->db->order_by('kategori','asc')->get('kategori_menu')->result();
	}

	function getDataKategoriMenuById($id){
		return $this->db->where('id',$id)->get('kategori_menu')->row();	
	}
	function insertKategoriMenu($field){
		$this->db->insert('kategori_menu',$field);
	}

	function updateKategoriMenu($id,$field){
		$this->db->where('id',$id)->update('kategori_menu',$field);
	}

	function deleteKategoriMenu($id){
		$this->db->where('id', $id)->delete('kategori_menu');
	}
}

/* End of file modelMenus.php */
/* Location: ./application/models/modelMenus.php */
 ?>