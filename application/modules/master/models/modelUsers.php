<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelUsers extends CI_Model {

	function auth($username,$password){
		$cek = $this->db->where(
			[
				'username'=>$username,
				'password'=>md5($password)
			]
		)->get('users');

		return $cek ;
	}

	function getData(){
		return $this->db->order_by('nip','asc')->get('users')->result();
	}

	function getDataByNip($nip){
		return $this->db->where('nip',$nip)->get('users')->row();	
	}
	function insert($field){
		$this->db->insert('users',$field);
	}

	function update($nip,$field){
		$this->db->where('nip',$nip)->update('users',$field);
	}

	function delete($nip){
		$this->db->where('nip', $nip)->delete('users');
	}

	function checkToken($token){
		return $this->db->where('token',$token)->get('users')->num_rows();
	}
	

}

/* End of file modelUsers.php */
/* Location: ./application/modules/master/models/modelUsers.php */
 ?>